import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { IdshareService } from '../../services/idshare.service';
import { ChangesuggestionService } from '../../services/changesuggestion.service';

@Component({
  selector: 'app-storify',
  templateUrl: './storify.component.html',
  styleUrls: ['./storify.component.css']
})
export class StorifyComponent implements OnInit {
  public data: string = "";
  public changesuggestion: string = "";
  public url = "http://3.17.16.237:9090/api/get-suggestion";
  constructor(public httpClient: HttpClient, public idShare: IdshareService, public change_s: ChangesuggestionService) { }

  id: string;

  ngOnInit() {
    this.idShare.currId.subscribe(id => { this.id = id; })
    this.sugsData();
    this.subssugsData();
  }
  sugsData() {
    let params = new HttpParams().set('video_id', this.id);
    return this.httpClient.get(this.url, { params }).toPromise()
      .then(responseJson => {
        return responseJson;
      });

  }
  subssugsData() {
    this.sugsData().then(data => {
      this.data = data['s'];
    })
  }
  changeSuggestion() {
    let sugg = { video_id: this.id, suggest: this.changesuggestion };
    this.change_s.changeSuggest(sugg).subscribe(
      res => { console.log(res); this.data = res['s']; }
    )

  }

}
