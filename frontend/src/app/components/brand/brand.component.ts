import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

@Component({
  selector: 'app-brand',
  templateUrl: './brand.component.html',
  styleUrls: ['./brand.component.css']
})
export class BrandComponent implements OnInit {

  public url = "http://3.17.16.237:9090/api/get-details";
  public user = localStorage.getItem('_user_name_');
  public details = {};

  constructor(public httpClient: HttpClient) { }

  ngOnInit() {
    this.brandData();
    this.subsbrandData();
  }

  brandData() {
    let params = new HttpParams().set('username', this.user);
    return this.httpClient.get(this.url, { params }).toPromise()
      .then(responseJson => {
        return responseJson;
      });
  }
  subsbrandData() {
    this.brandData().then(data => {
      this.details = data;
    })

  }

}