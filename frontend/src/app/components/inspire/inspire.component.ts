import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IdshareService } from '../../services/idshare.service';
import { Top20shareService } from '../../services/top20share.service';

@Component({
  selector: 'app-inspire',
  templateUrl: './inspire.component.html',
  styleUrls: ['./inspire.component.css']
})
export class InspireComponent implements OnInit {

  top20: Array<any>;

  public show_peer_intel: boolean = false;
  public show_hashtags: boolean = true;

  public user = localStorage.getItem('_user_name_');

  posts: any = [];

  peers: any = [];

  peers1: any = [];

  data: any = [];

  public url = "http://3.17.16.237:9090/api/get-hashtags";
  public url1 = "http://3.17.16.237:9090/api/peer-list";
  public url2 = "http://3.17.16.237:9090/api/get-peer-intel";

  constructor(public httpClient: HttpClient, public top20share: Top20shareService, public idShare: IdshareService) { }
  ngOnInit() {
    this.top20share.currTop20.subscribe(top20 => { this.top20 = top20; });

    this.getPeers();
    this.getTweets();
  }
  getTweets() {
    this.show_hashtags = true;
    this.show_peer_intel = false;
    this.posts = [];
    this.top20.forEach(element => {
      return fetch(this.url,
        {
          method: "POST",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            top20: element
          })
        })
        .then(res => res.json())
        .then(res => {
          this.posts.push(res)
        });
    });
  }
  getPeers() {
    this.show_hashtags = false;
    this.show_peer_intel = true;
    return fetch(`${this.url1}?username=${this.user}`)
      .then(res => res.json())
      .then(res => {
        this.peers = res;
        this.getPeerIntel();

      });
  }

  tweets() {
    this.show_hashtags = true;
    this.show_peer_intel = false;
  }

  peer() {
    this.show_hashtags = false;
    this.show_peer_intel = true;
  }

  getPeerIntel() {
    this.peers1 = [];
    this.peers.forEach(element => {
      return fetch(`${this.url2}?peer=${element}`)
        .then(res => res.json())
        .then(res => {
          this.peers1.push(res);
        })

    });

  }

}
