import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { CloudData, CloudOptions, TagCloudComponent } from 'angular-tag-cloud-module';
import { IdshareService } from '../../services/idshare.service';
import { Top20shareService } from '../../services/top20share.service';
import { HttpParams, HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-playvideo',
  templateUrl: './playvideo.component.html',
  styleUrls: ['./playvideo.component.css']
})
export class PlayvideoComponent implements OnInit {
  public show_v: boolean = false;
  public show_v_info: boolean = false;
  public show_s: boolean = false;
  public show_i: boolean = false;
  @ViewChild(TagCloudComponent, { static: false }) tagCloudComponent: TagCloudComponent;

  options: CloudOptions = {
    // if width is between 0 and 1 it will be set to the size of the upper element multiplied by the value 
    width: 450,
    height: 400,
    overflow: false,
  };
  data: CloudData[] = [];

  top = [];

  top20: Array<any>;

  id: string;

  transcribe: string;

  public audio = {};

  constructor(public idShare: IdshareService, public top20share: Top20shareService, public httpClient: HttpClient) { }
  ngOnInit() {
    this.idShare.currId.subscribe(id => { this.id = id; });
    this.getAudioData();
    this.subscribeAudioData();
    console.log(this.top20);
    return fetch(`http://3.17.16.237:9090/api/tagcloud-metadata-onevideo?video_id=${this.id}`)
      .then(response => response.json())
      .then(responseJson => {
        responseJson.forEach(element => {
          if (element.weight > 8) {
            this.top.push(element.text);
          }
        });
        this.data = responseJson;
        this.top20 = this.top;
        this.newtop20(this.top20);
        this.tagCloudComponent.reDraw();

      });

  }

  getAudioData() {
    let params = new HttpParams().set('video_id', this.id);
    return this.httpClient.get(`http://3.17.16.237:9090/api/transcribe-metadata`, { params }).toPromise()
      .then(responseJson => {
        return responseJson;
      })
  }
  subscribeAudioData() {
    this.getAudioData().then(data => {
      this.audio = data;
    });

  }

  newtop20(top20) {
    this.top20share.changeTop20(top20);
  }

  showComp(name) {
    if (name === 'vidfy') {
      this.show_v = true;
      this.show_s = false;
      this.show_i = false;
      this.show_v_info = true;

    }
    if (name === 'storify') {
      this.show_v = false;
      this.show_s = true;
      this.show_i = false;
      this.show_v_info = false;
    }
    if (name === 'inspire') {
      this.show_v = false;
      this.show_s = false;
      this.show_i = true;
      this.show_v_info = false;
    }

  }

}

