import { Component, OnInit, ViewChild } from '@angular/core';
import { CloudData, CloudOptions, TagCloudComponent } from 'angular-tag-cloud-module';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-tagcloud',
  templateUrl: './tagcloud.component.html',
  styleUrls: ['./tagcloud.component.css']
})
export class TagcloudComponent implements OnInit {
  @ViewChild(TagCloudComponent, { static: false }) tagCloudComponent: TagCloudComponent;
  options: CloudOptions = {
    // if width is between 0 and 1 it will be set to the size of the upper element multiplied by the value 
    width: 1000,
    height: 400,
    overflow: false,
  };

  public user = localStorage.getItem('_user_name_');
  data: CloudData[] = [];
  constructor() { }

  ngOnInit() {

    return fetch(`http://3.17.16.237:9090/api/tagcloud-metadata?username=${this.user}`)
      .then(response => response.json())
      .then(responseJson => {
        this.data = responseJson;
        this.tagCloudComponent.reDraw();

      });



  }

}
