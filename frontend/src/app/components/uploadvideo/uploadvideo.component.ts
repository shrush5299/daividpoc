import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { UploadingService } from '../../services/uploading.service';

@Component({
  selector: 'app-uploadvideo',
  templateUrl: './uploadvideo.component.html',
  styleUrls: ['./uploadvideo.component.css']
})
export class UploadvideoComponent implements OnInit {
  public show_msg: boolean = false;
  form: FormGroup;
  error: string;
  public status = "";
  uploadResponse = { status: '', message: '', filePath: '' };
  constructor(public formBuilder: FormBuilder, public uploadService: UploadingService) { }
  ngOnInit() {
    this.show_msg = false;
    this.form = this.formBuilder.group({
      file: ['']
    });
  }
  onFileChange(event) {
    this.show_msg = false;
    if (this.status == "Uploaded!") {
      this.status = "";
    }
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.form.get('file').setValue(file);
    }
  }
  onSubmit() {
    this.show_msg = true;
    const formData = new FormData();
    formData.append('file', this.form.get('file').value);
    this.uploadService.uploading(formData).subscribe(
      async res => //{ console.log(res); this.status = "Uploaded!"; }
      {
        console.log('#####');
        console.log(res);
        this.status = await res;
      }
    );
  }
}
