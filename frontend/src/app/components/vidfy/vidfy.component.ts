import { Component } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { MatSliderChange } from '@angular/material/slider';
import { IdshareService } from '../../services/idshare.service';


@Component({
  selector: 'app-vidfy',
  templateUrl: './vidfy.component.html',
  styleUrls: ['./vidfy.component.css']
})
export class VidfyComponent {

  id: string;

  public url = "http://3.17.16.237:9090/api/vidfy-metadata";
  data: any = {};
  data1: any;
  constructor(public httpClient: HttpClient, public idShare: IdshareService) { }
  ngOnInit() {
    this.idShare.currId.subscribe(id => { this.id = id; });
    this.getData();
    this.subsData();
  }
  getData() {
    let params = new HttpParams().set('video_id', this.id);
    return this.httpClient.get(this.url, { params })
      .toPromise()
      .then(responseJson => {
        return responseJson;
      });
  }
  subsData() {
    this.getData().then(data => {
      this.data = data;
    })
  }

  onInputChange(event: MatSliderChange) {
    this.data.vidfy.forEach(element => {
      if (element.timestamp == event.value) {
        let t = `Timestamp: ${element.timestamp}`;
        let f = '';
        let o = 'Objects: ';
        element.objects.forEach(ele => {
          let x = ele + ' ';
          o += x;
        });
        if ("face" in element) {
          f = `Faces: ${element.face}`;
        }
        let e = 'Emotions: ';
        if ("emotions" in element) {
          element.emotions.forEach(ele => {
            let x = ele + ' ';
            e += x;
          });
        }
        if (!("face" in element) && !("emotions" in element)) {
          this.data1 = t + '\n' + o;
        }
        else if (!("face" in element) && ("emotions" in element)) {
          this.data1 = t + '\n' + o + '\n' + e;
        }
        else if (("face" in element) && !("emotions" in element)) {
          this.data1 = t + '\n' + o + '\n' + f;
        }
        else {
          this.data1 = t + '\n' + o + '\n' + f + '\n' + e;
        }
        return;
      }
    });
  }
}


