import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { AuthService } from '../../services/auth.service';
import { AddpeerService } from '../../services/addpeer.service';
import { AddaudienceService } from '../../services/addaudience.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  public show_vid: boolean = false;
  public show_vidtc: boolean = false;
  public show_brand: boolean = false;
  public show_dash: boolean = true;
  public show_peers: boolean = false;
  public show_aud: boolean = false;
  public show_addasset: boolean = false;

  public data = {};

  public changepeer = "";
  public changeaudience = "";

  public url = "http://3.17.16.237:9090/api/get-details";
  public user = localStorage.getItem('_user_name_');
  public details = {};
  constructor(public auth: AuthService, public httpClient: HttpClient, public change_p: AddpeerService, public change_a: AddaudienceService) { }

  ngOnInit() {
    this.dashboardData();
    this.subsdashboardData();
  }
  peer() {
    this.show_peers = true;
    this.show_aud = false;
    this.show_addasset = false;
    this.dashboardData();
    this.subsdashboardData();
  }
  audience() {
    this.show_aud = true;
    this.show_peers = false;
    this.show_addasset = false;
    this.dashboardData();
    this.subsdashboardData();
  }
  asset() {
    this.show_addasset = true;
    this.show_aud = false;
    this.show_peers = false;
    this.dashboardData();
    this.subsdashboardData();
  }
  changeAudience() {
    if (this.changeaudience != "") {
      let p = { username: this.user, peer: this.changeaudience };
      this.show_aud = false;
      this.change_a.changeA(p).subscribe(
        res => {
          this.dashboardData();
          this.subsdashboardData();
        });
    }
  }
  changePeer() {
    if (this.changepeer != "") {
      let p = { username: this.user, peer: this.changepeer };
      this.show_peers = false;
      this.change_p.changeP(p).subscribe(
        res => {
          this.dashboardData();
          this.subsdashboardData();
        });
    }
  }

  showMenuOption(name) {
    if (name === 'dashboard') {
      this.show_vid = false;
      this.show_vidtc = false;
      this.show_brand = false;
      this.show_dash = true;
      this.dashboardData();
      this.subsdashboardData();
    }
    if (name === 'videos') {
      this.show_vid = true;
      this.show_vidtc = false;
      this.show_brand = false;
      this.show_dash = false;
    }
    if (name === 'videotagcloud') {
      this.show_vid = false;
      this.show_vidtc = true;
      this.show_brand = false;
      this.show_dash = false;
    }
    if (name === 'brand') {
      this.show_vid = false;
      this.show_vidtc = false;
      this.show_brand = true;
      this.show_dash = false;
    }

  }
  dashboardData() {
    let params = new HttpParams().set('username', this.user);
    return this.httpClient.get(this.url, { params }).toPromise()
      .then(responseJson => {
        return responseJson;
      });
  }
  subsdashboardData() {
    this.dashboardData().then(data => {
      this.details = data;
    })

  }

}

