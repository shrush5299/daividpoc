import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { IdshareService } from '../../services/idshare.service';


@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.css']
})
export class VideosComponent implements OnInit {

  public show_vids: boolean = true;
  public show_play: boolean = false;

  public url = "http://3.17.16.237:9090/api/retrieve-videos";
  public user = localStorage.getItem('_user_name_');
  public videos = {};

  id: string;

  constructor(public httpClient: HttpClient, public idShare: IdshareService) { }
  ngOnInit() {
    this.videosData();
    this.subsvideosData();
  }

  showMenuOption(name) {
    if (name === 'playvideo') {
      this.show_vids = false;
      this.show_play = true;
    }
    else if (name === 'uploadvideo') {
      this.show_vids = false;
      this.show_play = false;
    }
    else {
      this.show_vids = true;
      this.show_play = false;
    }
  }
  videosData() {
    let params = new HttpParams().set('username', this.user);
    return this.httpClient.get(this.url, { params }).toPromise()
      .then(responseJson => {
        return responseJson;
      });
  }
  subsvideosData() {
    this.videosData().then(data => {
      this.videos = data;
    })
  }
  newId(video_id) {
    this.idShare.changeId(video_id);
  }

}  
