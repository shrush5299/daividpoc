import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginData = {};
  constructor(public auth: AuthService, public router: Router) { }

  ngOnInit() {
  }

  loginUser() {
    this.auth.loginUsers(this.loginData).subscribe(
      res => {
        localStorage.setItem('token', res.token);
        localStorage.setItem('_user_name_', res.user_name);
        this.router.navigate(['/dashboard']);
      },
      err => console.log(err)
    )
  }
}

