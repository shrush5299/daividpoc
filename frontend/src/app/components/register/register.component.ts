import { Component, OnInit } from '@angular/core';
import { RegisteringService } from '../../services/registering.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  r = {};
  constructor(public reg: RegisteringService, public router: Router) { }
  ngOnInit() {

  }
  registerData() {
    this.reg.registerUser(this.r).subscribe(
      res => {
        console.log(res);
        this.router.navigate(['/']);
      },
    )

  }

}


