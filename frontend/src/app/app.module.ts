import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrandComponent } from './components/brand/brand.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { InspireComponent } from './components/inspire/inspire.component';
import { LoginComponent } from './components/login/login.component';
import { OptionsComponent } from './components/options/options.component';
import { PlayvideoComponent } from './components/playvideo/playvideo.component';
import { RegisterComponent } from './components/register/register.component';
import { StorifyComponent } from './components/storify/storify.component';
import { TagcloudComponent } from './components/tagcloud/tagcloud.component';
import { UploadvideoComponent } from './components/uploadvideo/uploadvideo.component';
import { VideosComponent } from './components/videos/videos.component';
import { VideotagcloudComponent } from './components/videotagcloud/videotagcloud.component';
import { VidfyComponent } from './components/vidfy/vidfy.component';

import { TagCloudModule } from 'angular-tag-cloud-module';
import {
  MatInputModule, MatPaginatorModule,
  MatSliderModule, MatTableModule, MatStepperModule, MatDialogModule
} from '@angular/material';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { VidfytimelineService } from './services/vidfytimeline.service';
import { AuthGuard } from './services/auth.guard';
import { AuthService } from './services/auth.service';
import { TokeninterceptorService } from './services/tokeninterceptor.service';
import { IdshareService } from './services/idshare.service';
import { Top20shareService } from './services/top20share.service';
import { ChangesuggestionService } from './services/changesuggestion.service';
import { TranscribeService } from './services/transcribe.service';
import { UploadingService } from './services/uploading.service';

@NgModule({
  declarations: [
    AppComponent,
    BrandComponent,
    DashboardComponent,
    InspireComponent,
    LoginComponent,
    OptionsComponent,
    PlayvideoComponent,
    RegisterComponent,
    StorifyComponent,
    TagcloudComponent,
    UploadvideoComponent,
    VideosComponent,
    VideotagcloudComponent,
    VidfyComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    TagCloudModule,
    MatStepperModule,
    MatInputModule,
    MatPaginatorModule,
    MatSliderModule,
    MatTableModule,
    MatDialogModule
  ],
  providers: [VidfytimelineService, AuthGuard, AuthService, TranscribeService, UploadingService,
    TokeninterceptorService, IdshareService, Top20shareService, ChangesuggestionService, {
      provide: HTTP_INTERCEPTORS,
      useClass: TokeninterceptorService,
      multi: true
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }
