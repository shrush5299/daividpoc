import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RegisteringService {

  public url = "http://3.17.16.237:9090/api/register";
  constructor(public httpClient: HttpClient) { }

  registerUser(user) {
    return this.httpClient.post<any>(this.url, user);
  }
}
