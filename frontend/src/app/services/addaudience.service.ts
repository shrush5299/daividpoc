import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AddaudienceService {
  public url = "http://3.17.16.237:9090/api/add-audience";

  constructor(public httpClient: HttpClient) { }

  changeA(data) {
    return this.httpClient.post<any>(this.url, data);
  }
}

