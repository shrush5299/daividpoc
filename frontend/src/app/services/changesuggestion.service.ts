import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ChangesuggestionService {

  public url = "http://3.17.16.237:9090/api/change-suggestion";

  constructor(public httpClient: HttpClient) { }

  changeSuggest(data) {
    return this.httpClient.post<any>(this.url, data);
  }
}
