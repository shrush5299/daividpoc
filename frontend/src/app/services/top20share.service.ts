import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class Top20shareService {
  public top20Source = new BehaviorSubject<Array<any>>([]);
  currTop20 = this.top20Source.asObservable();

  constructor() { }

  changeTop20(top20: Array<any>) {
    this.top20Source.next(top20);
  }
}

