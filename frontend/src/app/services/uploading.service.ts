import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpErrorResponse, HttpEventType } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UploadingService {

  SERVER_URL: string = "http://18.204.228.252:5000";
  NODE_URL: string = "http://3.17.16.237:9090";
  constructor(public httpClient: HttpClient) { }
  transcribe = null;
  fileName = null;
  public up: string;

  trans(data) {
    let uploadURL1 = `${this.SERVER_URL}/analyse-audio`;
    return fetch(uploadURL1,
      {
        method: "POST",
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: data,
      })
    // .then(res => res.json())
    // .then(res => {
    //   console.log(res);
    //   //this.getTranscribe();
    // })
  }
  // getTranscribe() {
  //   return fetch(`${this.SERVER_URL}/extract-audio-text?file=${this.fileName}`)
  //     .then(res => res.json())
  //     .then(res => {
  //       this.transcribe = res;
  //     })
  // }

  public uploading(data) {
    let uploadURL = `${this.SERVER_URL}/analyse-video`;
    let objects, faces = null;
    let file_convert = data.get('file');
    var file_converted = null;
    let read = new FileReader();
    read.readAsDataURL(file_convert);
    read.onload = function () {
      file_converted = read.result;
    }
    return this.httpClient.post<any>(uploadURL, data, {
      reportProgress: true,
      observe: 'events'
    }).pipe(map((async event => {
      switch (event.type) {
        // case HttpEventType.UploadProgress:
        //   const prog = Math.round(100 * event.loaded / event.total);
        //   return { status: 'progress', message: prog };
        case HttpEventType.Response:
          var f = JSON.stringify(event.body[0]);
          var ff = JSON.parse(f);
          this.fileName = ff.file;
          //console.log(file_converted);
          return fetch(`${this.SERVER_URL}/extract-labels?file=${this.fileName}`)
            .then(response => response.json())
            .then(responseJson => {
              objects = responseJson[0]['object-detected']['labels'];
              return fetch(`${this.SERVER_URL}/extract-face-analysis?file=${this.fileName}`);
            })
            .then(response => response.json())
            .then(responseJson => {
              faces = responseJson[0]['face-analysis']['labels'];
              this.trans(data);
              return fetch(`${this.SERVER_URL}/extract-audio-text?file=${this.fileName}`);
            })
            .then(response => response.json())
            .then(response => {
              this.transcribe = response;
              return fetch(`${this.NODE_URL}/api/upload-video-metadata`,
                {
                  method: "POST",
                  headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                  },
                  body: JSON.stringify({
                    o: objects,
                    f: faces,
                    d: file_converted,
                    t: this.transcribe,
                    u: localStorage.getItem('_user_name_')
                  })
                })
                .then(res => res.json())
                .then(res => {
                  this.up = res.msg;
                  return `Uploaded!`;
                });

            });
        default:
          // console.log(`${event.type}`);
          // console.log(`Unhandled event: ${event.type}`);
          //return `Unhandled event: ${event.type}`;
          return `Uploading... Please wait, it won't take long!`;
      }
    })));
  }

}
