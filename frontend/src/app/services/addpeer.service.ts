import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AddpeerService {
  public url = "http://3.17.16.237:9090/api/add-peer";

  constructor(public httpClient: HttpClient) { }

  changeP(data) {
    return this.httpClient.post<any>(this.url, data);
  }
}
