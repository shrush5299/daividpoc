import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class IdshareService {
  public idSource = new BehaviorSubject<string>("");
  currId = this.idSource.asObservable();


  constructor() { }

  changeId(id: string) {
    this.idSource.next(id);
  }
}
