import { Injectable, Injector } from '@angular/core';
import { HttpInterceptor } from '@angular/common/http';
import { AuthService } from './auth.service';


@Injectable({
  providedIn: 'root'
})


export class TokeninterceptorService implements HttpInterceptor {

  constructor(public injector: Injector) { }

  intercept(req, next) {
    let authService = this.injector.get(AuthService);
    let tokenized_req = req.clone({
      setHeaders: {
        Authorization: `Bearer ${authService.getToken()}`
      }
    })
    return next.handle(tokenized_req);
  }
}
