const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const videoSchema = new Schema({
    username: {
        type: String,
        required: true
    },
    video: {
        type: String,
        required: true
    },
    objects: {
        type: Array,
        required: true
    },
    faces: {
        type: Array,
        required: true
    },
    transcribe: {
        type: Array,
    },
    suggestion: {
        type: String,
        default: ""
    }
});

module.exports = Video = mongoose.model('videos', videoSchema);