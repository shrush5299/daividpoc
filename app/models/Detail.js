const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const detailSchema = new Schema({
    username: {
        type: String,
        required: true
    },
    brandname: {
        type: String,
        required: true
    },
    assets: {
        type: Number,
        required: true
    },
    audience: {
        type: Array
    },
    peers: {
        type: Array
    },
    about: {
        type: String,
        required: true
    }
});

module.exports = Detail = mongoose.model('details', detailSchema);