const router = require('express').Router();
const User = require('./models/User');
const Video = require('./models/Video');
const Detail = require('./models/Detail');
const key = 'Xub2hToUKcr43qPmiRWRJh0zTp3UzgPT';
const jwt = require('jsonwebtoken');
const nodemailer = require('nodemailer');

const twit = require('twit');
var T = new twit({
    consumer_key: 'MeRdecMB22apg2CkJzifrIikt',
    consumer_secret: 'cR2ku37U9huGmDL9owNrYd0hhrVOtaUB31gMWlB9cw8KWFz6Bp',
    access_token: '1042797524644290560-mzyTErAuz1TA6tIVn9siked500sc9f',
    access_token_secret: '9TThMpTlrnvJ2Kbonu2goLbZJTQ60F3St8Hxn6m6cuWWh',
    timeout_ms: 60 * 1000
});
var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: 'mail.register.daivid@gmail.com',
        pass: 'daivid1234'
    }
});

/*
*
* LOGGING IN, REGISTERING, FETCHING USER VALUES
*
*/
router.post('/register', async (req, res) => {
    let { email, username, brandname, about, password } = req.body
    // Check for the unique email
    await User.findOne({ email }).then(user => {
        if (user) return res.status(401).json({
            message: 'The username or email id is taken. Please try with a different username or email id!',
            success: false
        });

        let newUser = new User({
            username,
            email,
            password
        });
        var mailOptions = {
            from: 'mail.register.daivid@gmail.com',
            to: req.body.email,
            subject: 'Welcome to Daivid',
            html: 'Thanks for registering to Daivid!'
        };
        newUser.save((error, user) => {
            if (error) {
                console.log("Error");
            }
            else {
                let newDetails = new Detail({
                    username,
                    brandname,
                    about,
                    assets: 0
                });
                Detail.create(newDetails);
                transporter.sendMail(mailOptions, function (err, info) {
                    if (err) { console.log(err); }
                    else {
                        console.log("Sent: " + info.response);
                    }
                });
                res.status(201).send(user);
            }
        });
        /*bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(newUser.password, salt, (err, hash) => {
                if (err) throw err;
                newUser.password = hash;
                newUser.save().then(user => {
                    return res.status(201).json({
                        success: true,
                        msg: "User is registered successfully!"
                    });
                });
            });
        });*/
    });
});

router.post('/login', (req, res) => {
    let userData = req.body;
    User.findOne({ email: userData.email }, (err, user) => {
        if (err) {
            console.log(err);
        }
        else {
            if (user.password != userData.password && user.password != userData.password) {
                res.status(401).send('Invalid Credentials');
            }
            else {
                const token = jwt.sign({
                    id: user._id, username: user.username
                }, key, { expiresIn: '1hr' });
                res.status(200).json({ user_name: userData.username, token: token }
                );

            }
        }
    });
});

router.get('/get-details', async (req, res) => {
    await Detail.find({ username: req.query.username }).then(details => {
        return res.status(200).json(details);
    });
});
router.post('/add-peer', async (req, res) => {
    Detail.findOneAndUpdate({ username: req.body.username },
        {
            $push: {
                peers: req.body.peer
            }
        },
        { new: true },
        function (err, result) {
            if (err) { console.log("err"); }
            else if (result) { console.log("result"); }
        });
    return res.status(200).send(console.log("added"));
});
router.post('/add-audience', async (req, res) => {
    Detail.findOneAndUpdate({ username: req.body.username },
        {
            $push: {
                audience: req.body.peer
            }
        },
        { new: true },
        function (err, result) {
            if (err) { console.log("err"); }
            else if (result) { console.log("result"); }
        });
    return res.status(200).send();
});
/*
*
* UPLOADING VIDEO AND  STORING VALUES FROM
*   1)analyse video, object detector and face analyser APIs
*   2)analyse audio and Transcribe API
*/
router.post('/upload-video-metadata', async (req, res) => {
    console.log("Backend accessed");
    var obj = req.body.o;
    var face = req.body.f;
    var vid = req.body.d;
    var trans = req.body.t;
    var userr = req.body.u;
    let newVid = new Video({
        username: userr,
        objects: obj,
        faces: face,
        video: vid,
        transcribe: trans
    });
    newVid.save().then(user => {
        console.log("video-u");
        Detail.findOneAndUpdate(
            { username: userr },
            {
                $inc: { assets: 1 }
            },
            function (err, result) {
                if (err) { console.log(err); }
                else { console.log("done"); }
            });
        return res.status(201).json({
            success: true,
            msg: "Video successfully uploaded!"
        });
    });
    console.log("heyo");
});
/*
*
*  VIDFY, STORIFY, INSPIRE
*
*/
router.get('/vidfy-metadata', async (req, res) => {
    await Video.findById(req.query.video_id).then(video => {
        var f = video.faces;
        var o = video.objects;
        var t = [];
        var t1 = [];
        f.forEach(element => {
            t.push(element.timestamp);
        });
        o.forEach(element => {
            t1.push(element.timestamp);
        });
        var t2 = t.concat(t1);
        const distinct = (value, index, self) => {
            return self.indexOf(value) === index;
        }
        t2 = t2.filter(distinct);
        function sortNumber(a, b) {
            return a - b;
        }
        t3 = t2.sort(sortNumber);
        var vidfy = [];
        //console.log("Hiii");
        t3.forEach(i => {
            //console.log("Hiii");
            objs = [];
            var emo = [];
            let gen;
            o.forEach(element => {
                if (element.timestamp == i && element.confidence > 65) {
                    objs.push(element.name);
                }
            });
            f.forEach(element => {
                if (element.timestamp == i) {
                    if (element.Smile.Value == true) {
                        emo.push('Smile');
                    }
                    if (element.Gender.Value == "Female") {
                        gen = "Female";
                    }
                    else if (element.Gender.Value == "Male") {
                        gen = "Male";
                    }
                    else {
                        gen = "None";
                    }
                    x = element.Emotions;
                    x.forEach(ele => {
                        if (ele.Confidence > 90) {
                            l = ele.Type.toLowerCase();
                            emo.push(l);
                        }
                    });

                }
            });
            let a;
            if (emo.length < 1) {
                if (gen == "None") {
                    a = { 'timestamp': i, 'objects': objs };
                }
                else {
                    a = { 'timestamp': i, 'face': gen, 'objects': objs };
                }
            }
            else {
                a = { 'timestamp': i, 'face': gen, 'objects': objs, 'emotions': emo };
            }
            vidfy.push(a);
            i = i + 200;
        });
        return res.status(201).json({
            max: t3[t3.length - 1], vidfy
        });
    });
});

function randomColor() {
    let letters = "0123456789abcdef";
    let color = '#';
    for (var i = 0; i < 6; i++)
        color += letters[(Math.floor(Math.random() * 16))];
    return color;
}
function contains(arr, key, val) {
    for (var i = 0; i < arr.length; i++) {
        if (arr[i][key] === val) return true;
    }
    return false;
}

router.get('/tagcloud-metadata', async (req, res) => {
    await Video.find({ username: req.query.username }).then(videos => {
        let o1 = [];
        videos.forEach(video => {
            let o = video.objects;
            o.forEach(element => {
                if (!(contains(o1, "text", element.name))) {
                    o1.push({ text: element.name, weight: Math.round(element.confidence * 0.1), color: randomColor() });
                }
            });
        });

        return res.json(o1);
    });
});

router.get('/tagcloud-metadata-onevideo', async (req, res) => {
    await Video.findById(req.query.video_id).then(video => {
        let o1 = [];
        let o = video.objects;
        var sug = '';
        o.forEach(element => {
            if (!(contains(o1, "text", element.name))) {
                o1.push({ text: element.name, weight: Math.round(element.confidence * 0.1), color: randomColor() });
                let x = Math.round(element.confidence) * 0.1;
                if (x >= 8) {
                    sug = sug.concat(element.name);
                    sug = sug.concat(' ');
                }
            }
        });

        if (video.suggestion == "") {
            let query = { _id: req.query.video_id }
            Video.findOneAndUpdate(
                query,
                {
                    $set: { suggestion: sug }
                },
                { new: true },
                function (err, result) {
                    if (err) {
                        console.log("err");
                    }
                    else if (result) {
                        console.log("result");
                    }
                }
            );
        }
        return res.status(200).json(o1);
    });
});
router.get('/transcribe-metadata', async (req, res) => {
    await Video.findById(req.query.video_id).then(video => {
        let r1 = video.transcribe;
        let r = r1[0]["results"]["transcripts"][0].transcript;
        return res.status(200).json({ "text": r });
    });

});

router.get('/get-suggestion', async (req, res) => {
    await Video.findById(req.query.video_id).then(video => {
        return res.status(200).json({ s: video.suggestion });
    });
});


router.post('/change-suggestion', async (req, res) => {
    let ss = req.body.suggest;
    Video.findOneAndUpdate({ _id: req.body.video_id },
        {
            $set: { suggestion: req.body.suggest }
        },
        { new: true },
        function (err, result) {
            if (err) { console.log("err"); }
            else { console.log("updated"); }
        }
    );
    return res.status(200).json({ s: ss });
});
// router.post('/addtrans', async (req, res) => {
//     Video.findOneAndUpdate({ _id: "5d260ecf9e030f3bdcd90c68" },
//         {
//             $set: { transcribe: req.body.trans }
//         },
//         { new: true },
//         function (err, result) {
//             if (err) { console.log("err"); }
//             else { console.log("updated"); }
//         });
//     return res.status(200).send("done");

// });

router.post('/get-hashtags', async (req, res) => {
    T.get('search/tweets', { q: req.body.top20 }, function (err, data, response) {
        ob2 = data;
        return res.status(200).json(data.statuses);
    });
});

router.get('/peer-list', async (req, res) => {
    Detail.findOne({ username: req.query.username }).then(video => {
        return res.status(200).json(video.peers);
    });

});


router.get('/get-peer-intel', async (req, res) => {
    let options = { screen_name: req.query.peer, count: 5 }
    T.get('statuses/user_timeline', options, function (err, data, response) {
        return res.status(200).json(data);
    });

});
/*
*
*  VIDEOS
*
*/
router.get('/get-video', async (req, res) => {
    Video.findOneAndUpdate({ _id: req.query.video_id }, {}).then((videos) => {
        res.status(200).json(videos);

    });
});
router.get('/retrieve-videos', async (req, res) => {
    await Video.find({ username: req.query.username }).then((videos) => {
        let vid = [];
        videos.forEach(element => {
            vid.push({ video: element.video, video_id: element._id });
        });
        return res.status(200).json(vid);
    });
});

module.exports = router;