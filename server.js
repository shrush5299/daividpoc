const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const path = require('path');
const passport = require('passport');
const db = require('./config/config').db; // Connect with the database
const key = 'Xub2hToUKcr43qPmiRWRJh0zTp3UzgPT';

mongoose.set('useFindAndModify', false);
mongoose.connect(db, { useNewUrlParser: true }) // Connect with the mongodb
    .then(() => console.log(`Database connected successfully ${db}`))
    .catch(err => console.log(err));
const PORT = process.env.PORT || 9090;
const app = express();
app.use(cors());
app.use(bodyParser.json({ limit: '30mb', extended: true }))
app.use(bodyParser.urlencoded({ limit: '30mb', extended: true }))
app.use(passport.initialize());

const requests = require('./app/apis');
app.use('/api', requests);
// app.get('/', (req, res) => {
//     res.status(200).json({
//         message: "Hello World",
//         success: true
//     });
// });
// app.get('*', async (req, res) => {
//     res.sendFile('frontend/src/index.html', { root: __dirname });
// });
app.listen(PORT, () => {
    console.log(`Server started on port ${PORT}`)
});